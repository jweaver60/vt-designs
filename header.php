<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>

	<title><?php bloginfo('name'); is_front_page() ? bloginfo('description') : wp_title('|'); ?></title>
	
	<link rel="shortcut icon" href="/favicon.ico">
	
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>

	<?php wp_head(); ?>

	<script src="<?php bloginfo('template_url'); ?>/js/responsive-nav.min.js"></script>

	<script src="<?php bloginfo('template_url'); ?>/js/custom.js"></script>

</head>

<body <?php body_class(); ?>>
	
	<header class="<?php get_header_class(); ?>">
		<div class="container">
			<div class="top-tools">
<!-- 				<div class="social-container">
					<a href="#" class="linkedin-button">LinkedIn</a>
				</div> -->
				<div class="search-container">
					<input type="text" name="search" class="searchbox" />
					<a href="#" class="search-button"><i class="icon-search"></i></a>
				</div>
			</div>
		</div>
		<nav class="main-nav">
			<div class="container">
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
				<ul>
					<li class="hide-mobile"><a href="#">Menu</a></li>
				</ul>
			</div>
		</nav>
		<div class="container">
			<nav class="mobile-nav">
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
			</nav>
		</div>
		<div class="container">
			<div class="logo">
				<a href="<?php echo home_url(); ?>">
					<img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="VT Designs Logo" />
				</a>
			</div>
		</div>
		<?php // if(is_front_page()) { ?>
			<div class='slider'>
				<div class='slider-content'>
					<h4>Our Experience</h4>
					<h4 class='inverse'>Your Engineering Advantage</h4>
					<!-- <h5>Sem Pellentesque Quamaugue</h5> -->
				</div>
				<div class='diagonal'></div>
			</div>
			<nav class="sub-nav">
				<div class="container">
					<ul>
						<li><a class="commercial" href="<?php echo site_url(); ?>/commercial">Commercial</a></li>
						<li><a class="education" href="<?php echo site_url(); ?>/education">Education</a></li>
						<li><a class="government" href="<?php echo site_url(); ?>/government">Government</a></li>
						<li><a class="healthcare" href="<?php echo site_url(); ?>/healthcare">Healthcare</a></li>
						<li><a class="residential" href="<?php echo site_url(); ?>/residential">Residential</a></li>
						<li><a class="manufacturing" href="<?php echo site_url(); ?>/manufacturing">Manufacturing</a></li>
					</ul>
				</div>
			</nav>
		<?php // } ?>
	</header>

	<div class="main">