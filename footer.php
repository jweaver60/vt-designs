	</div><!-- end main -->
	
	<footer>
		<div class="footer-inner">
			<div class="container">
				<div class="row">
					<div class="five columns">
						<h3 class="inverse"><a href="<?php bloginfo('template_url'); ?>/contact" style="text-decoration: none;">Contact Us</a></h3>
						<p>155 Tri-County Parkway, Suite 230<br />
							Cincinnati, OH 45246</p>
						<ul class="with-margin">
							<li><span class="bold pad">P</span>(513) 772-1756</li>
							<li><span class="bold pad">F</span>(513) 772-5748</li>
						</ul>
					</div>
					<div class="four columns offset-by-one">
						<p class="top-spacing">5200 Sprinfield St., Suite 320<br />
							Dayton, OH 45431</p>
						<ul class="with-margin">
							<li><span class="bold pad">P</span>(937) 476-2262</li>
							<li><span class="bold pad">E</span><a href="mailto:info@vt-designs.com">info@vt-designs.com</a></li>
						</ul>
					</div>
					<div class="five columns offset-by-one" style="padding-top: 1em;">
						<a href="#"><img src="<?php bloginfo('template_url'); ?>/images/go_green.png" alt="Go Green Challenge" /></a>
						<a href="#"><img src="<?php bloginfo('template_url'); ?>/images/green_building_council.png" alt="Green Building Council" width="75" height="75" /></a>
					</div>
				</div>
				<div class="footer-links">
					<ul>
<!-- 						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Terms and Conditions</a></li>
						<li><a href="#">Sitemap</a></li>
						<li><a href="#">FAQ</a></li> -->
						<li>Copyright &copy;<?php echo ("2014"); echo " "; bloginfo('name'); ?></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

	<?php wp_footer(); ?>
	
	<!-- Don't forget analytics -->
	
</body>

</html>
