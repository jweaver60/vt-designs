<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="ten columns">
			<?php query_posts('cat=-3,-4,-5'); ?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<section <?php post_class() ?> id="post-<?php the_ID(); ?>">

					<h5><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>

					<?php // include (TEMPLATEPATH . '/inc/meta.php' ); ?>

					<div class="entry">
						<?php the_content('continued...'); ?>
					</div>

					<!--<div class="postmetadata">
						<?php // the_tags('Tags: ', ', ', '<br />'); ?>
						Posted in <?php // the_category(', ') ?> | 
						<?php // comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
					</div>-->

				</section>

			<?php endwhile; ?>

			<?php // include (TEMPLATEPATH . '/inc/nav.php' ); ?>

			<?php else : ?>

				<h2>Not Found</h2>

			<?php endif; ?>
		</div>
		<div class="five columns offset-by-one background">
			<?php get_sidebar('news'); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>