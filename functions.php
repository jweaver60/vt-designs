<?php
	
	// Add RSS links to <head> section
	automatic_feed_links();
	
	// Load jQuery
	if ( !is_admin() ) {
	  wp_deregister_script('jquery');
	  wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"), false);
	  wp_enqueue_script('jquery');
	}
	
	// Clean up the <head>
	function removeHeadLinks() {
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
  }
  add_action('init', 'removeHeadLinks');
  remove_action('wp_head', 'wp_generator');
    
	// Declare sidebar widget zone
  if (function_exists('register_sidebar')) {
    register_sidebar(array(
    	'name' => 'Sidebar Widgets',
    	'id'   => 'sidebar-widgets',
    	'description'   => 'These are widgets for the sidebar.',
    	'before_widget' => '<div id="%1$s" class="widget %2$s">',
    	'after_widget'  => '</div>',
    	'before_title'  => '<h2>',
    	'after_title'   => '</h2>'
    ));
  }

  function register_my_menu() {
      register_nav_menu('header-menu',__( 'Header Menu' ));
  }
  add_action( 'init', 'register_my_menu' );

  add_theme_support( 'post-thumbnails' ); 

  function get_excerpt_by_id($post_id){
    $the_post = get_post($post_id);
    $the_excerpt = $the_post->post_content;
    $excerpt_length = 55; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);
    if(count($words) > $excerpt_length) :
      array_pop($words);
      array_push($words, '…');
      $the_excerpt = implode(' ', $words);
    endif;
    return $the_excerpt;
  }

  function get_header_class() {
    if (is_page('about') || is_page('portfolio')) {
      echo 'internal_gallery';
    } elseif (is_page('services')) {
      echo 'contact-header';
    } elseif (is_home()) {
      echo 'internal';
    } elseif (is_front_page()) {
      echo '';
    } else {
      echo 'contact-header';
    }
  }

?>