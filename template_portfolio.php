<?php 
/* 
Template Name: Portfolio Page Template
*/
?>

<?php get_header(); ?>

<div class="container">
	<div class="top-caption">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; endif; ?>
	</div>
</div>
<div class="container">
		<?php
		global $post;
		$args = array( 'category' => 5, 'posts_per_page' => 10 );
		$myposts = get_posts( $args );
		foreach( $myposts as $post ) : setup_postdata($post); ?>
		<div class="row border">
			<div class="five columns">
				<?php if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				} ?>
			</div>
			<div class="eleven columns">
				<h3><?php the_title(); ?></h3>
				<?php the_content(); ?>
			</div>
		</div>
	<?php endforeach; ?>
</div><!-- end container -->

<?php get_footer(); ?>