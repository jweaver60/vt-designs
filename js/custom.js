$(document).ready(function() {

	$('.gallery img').click(function() {
		$(this).next('.port_desc').fadeIn('fast');
	});

	$(document).mouseup(function (e) {
    var container = $(".port_desc");
    if (container.has(e.target).length === 0) {
    	container.hide();
    }
	});

	$("li.hide-mobile").click(function() {
		$(".mobile-nav").toggle();
	});

});