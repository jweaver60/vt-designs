<?php 
/* 
Template Name: Home Page Template
*/
?>

<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="eight columns">
		
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
			<section class="post" id="post-<?php the_ID(); ?>">

				<?php if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				} ?>

				<div class="entry">

					<?php the_content(); ?>

				</div>

			</section>

			<?php endwhile; endif; ?>

		</div>
		<div class="seven columns offset-by-one background">

			<?php get_sidebar('home'); ?>

		</div>
	</div>
</div>
<div class="container">
	<h4 class="lower-caption">VT Design Solutions is a certified Minority Business Enterprise (MBE), SBE and a Veteran-Owned Small Business (VOSB).</h4>
</div>

<?php get_footer(); ?>