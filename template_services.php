<?php 
/* 
Template Name: Services Page Template
*/
?>

<?php get_header(); ?>

<div class="container">
	<div class="seperators">
		<h1>What We Do</h1>
	</div>
</div>
<div class="container">
	<div class="row extra-bottom">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; endif; ?>
	</div>
</div><!-- end container -->

<?php get_footer(); ?>