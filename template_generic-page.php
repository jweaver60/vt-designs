<?php 
/* 
Template Name: Generic Page Template
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="container">
		<div class="seperators">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
	<div class="container">
		<div class="row extra-bottom generic-template">
			<?php the_content(); ?>
		</div>
	</div><!-- end container -->
<?php endwhile; endif; ?>

<?php get_footer(); ?>