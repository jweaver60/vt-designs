<section class="background-border">
	<h5 class="inverse extra-top">Recent Posts</h5>
	<nav class="categories">
		<ul>
			<?php
				$args = array( 'numberposts' => '10', 'category' => 1 );
				$recent_posts = wp_get_recent_posts( $args );
					foreach( $recent_posts as $recent ){ ?>
						<li><a href="<?php echo get_permalink($recent["ID"]); ?>"><?php echo $recent["post_title"]; ?></a></li>
			<?php } ?>
		</ul>
	</nav>
</section>